package fizzbuzz;

class C {
    public static void main(String[] a) {
        for (int i = 1; i < 101; i++) {
            System.out.println((i % 3 < 1 ? "Fizz" : "") + (i % 5 < 1 ? "Buzz" : i % 3 < 1 ? "" : i));
        }
    }
}