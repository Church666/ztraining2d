package fizzbuzz;

class F {
    public static void main(String[] s) {
        for (int i = 1; i < 101; i++) {
            System.out.println((i % 3 < 1 ? "Fizz" + (i % 5 < 1 ? "Buzz" : "") : (i % 5 < 1 ? "Buzz" : i)));
        }
    }
}