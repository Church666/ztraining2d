package fizzbuzz;

class G {
    public static void main(String[] a) {
        for (int i = 0; i++ < 100;) {
            String s = (i % 3 == 0) ? "Fizz" : "";
            if (i % 5 == 0)
                s += "Buzz";
            System.out.println((s == "") ? i : s);
        }
    }
}