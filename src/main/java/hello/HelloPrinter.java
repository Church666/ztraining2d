package hello;

public class HelloPrinter implements Printer {
    private HelloWorld hello = new HelloWorld();

    @Override
    public void print() {
        hello.hello();
    }

}
