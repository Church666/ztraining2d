package montyhall.takagi;

import montyhall.takagi.strategy.IStrategy;

/**
 * プレイヤークラス.
 * @author d450131
 */
public class Player {

    /** プレイヤーの保持する封筒. */
    private Envelope envelope;

    /** プレイヤーの用いる戦略. */
    private IStrategy strategy;

    /**
     * コンストラクタ.
     * 
     * @param strategy 戦略
     */
    public Player(IStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * 戦略にもとづき、封筒を取得する.
     * @param chairperson 司会者
     * @return プレイチャーの選択した封筒
     */
    public void getEnvelope(Chairperson chairperson) {
        envelope = strategy.getEnvelope(chairperson);
    }

    /**
     * 封筒を取得する.
     * @return
     */
    public Envelope getEnvelope() {
        return envelope;
    }

    /**
     * 戦略を取得する.
     * @return 戦略
     */
    public IStrategy getStrategy() {
        return strategy;
    }
}
