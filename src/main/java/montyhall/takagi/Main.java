package montyhall.takagi;

import montyhall.takagi.strategy.ChoiceAgainStrategy;
import montyhall.takagi.strategy.NotChoiceAgainStrategy;

/**
 * ゲームを実行するメインクラス.
 * @author d450131
 */
public class Main {

    private static final int DEFAULT_LOOP_NUMBER = 1000000;

    public static void main(String[] args) {

        // ループ回数の設定(引数なしの場合はデフォルト回数をセット)
        int loopNumber = DEFAULT_LOOP_NUMBER;
        if (args != null && args.length > 0) {
            loopNumber = Integer.parseInt(args[0]);
        }

        Game game1 = new Game(new Chairperson(), new Player(new ChoiceAgainStrategy()), loopNumber);
        game1.start();

        Game game2 = new Game(new Chairperson(), new Player(new NotChoiceAgainStrategy()), loopNumber);
        game2.start();
    }
}
