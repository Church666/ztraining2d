package montyhall.takagi;

/**
 * ゲームクラス.
 * @author d450131
 */
public class Game {

    /** 司会者. */
    private Chairperson chairperson;

    /** プレイヤー. */
    private Player player;

    /** ループ回数. */
    private int loopNumber;

    /**
     * コンストラクタ.
     * @param chairperson 司会者
     * @param player プレイヤー
     * @param loopNumber ゲーム内のループ回数
     */
    public Game(Chairperson chairperson, Player player, int loopNumber) {
        this.chairperson = chairperson;
        this.player = player;
        this.loopNumber = loopNumber;
    }

    /**
     * ゲームを実施する.
     */
    public void start() {
        System.out.println("■■■ Game Start! ■■■");

        int winNumber = 0;
        int loseNumber = 0;

        for (int i = 0; i < loopNumber; i++) {
            chairperson.prepareEnvelopes();

            // 司会から封筒を取得
            player.getEnvelope(chairperson);

            // 封筒の中身を確認
            if (Envelope.HIT.equals(player.getEnvelope())) {
                winNumber++;
            } else {
                loseNumber++;
            }
        }

        System.out.println(player.getStrategy().getClass().getName() + ":total[" + loopNumber + "]" + " win["
            + winNumber + "] lose[" + loseNumber + "]");
        System.out.println("■■■ Game End! ■■■");
    }
}
