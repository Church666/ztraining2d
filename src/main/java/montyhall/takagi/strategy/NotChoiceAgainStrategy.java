package montyhall.takagi.strategy;

import montyhall.takagi.Chairperson;
import montyhall.takagi.Envelope;

/**
 * 封筒を再選択しない戦略クラス.
 * @author d450131
 */
public class NotChoiceAgainStrategy implements IStrategy {

    @Override
    public Envelope getEnvelope(Chairperson chairperson) {

        // 司会者から封筒を取得する
        return chairperson.getAndRemoveEnvelope();
    }
}
