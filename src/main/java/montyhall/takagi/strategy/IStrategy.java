package montyhall.takagi.strategy;

import montyhall.takagi.Chairperson;
import montyhall.takagi.Envelope;

/**
 * 戦略インターフェース.
 * @author d450131
 */
public interface IStrategy {

    /**
     * 戦略にもとづき、封筒を取得する.
     * @param chairperson 司会者
     * @return 封筒
     */
    public Envelope getEnvelope(Chairperson chairperson);
}
