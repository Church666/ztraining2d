package montyhall.takagi.strategy;

import montyhall.takagi.Chairperson;
import montyhall.takagi.Envelope;

/**
 * 封筒を再選択する戦略クラス.
 * @author d450131
 */
public class ChoiceAgainStrategy implements IStrategy {

    @Override
    public Envelope getEnvelope(Chairperson chairperson) {

        // 司会者から封筒を取得する
        Envelope result = chairperson.getAndRemoveEnvelope();

        // 司会者がハズレの封筒を削除する
        chairperson.removeMissEnvelope();

        // 司会者から封筒を再取得する
        result = chairperson.getAndRemoveEnvelope();

        return result;
    }
}
