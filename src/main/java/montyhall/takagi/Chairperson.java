package montyhall.takagi;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 司会者クラス.
 * @author d450131
 */
public class Chairperson {

    /** 封筒. */
    private List<Envelope> envelopes;

    /**
     * 封筒を作成する.
     */
    public void prepareEnvelopes() {
        envelopes = new ArrayList<Envelope>();
        envelopes.add(Envelope.HIT);
        envelopes.add(Envelope.MISS);
        envelopes.add(Envelope.MISS);
    }

    /**
     * 封筒のうち、ランダムで1つを削除し、削除した封筒を返却する.
     * @return 削除した封筒
     */
    public Envelope getAndRemoveEnvelope() {
        Random random = new Random();

        // 現時点の封筒サイズの範囲の乱数を生成
        int randomNumber = random.nextInt(envelopes.size());

        return envelopes.remove(randomNumber);
    }

    /**
     * 封筒のうち、ハズレの封筒1つを削除する.
     */
    public void removeMissEnvelope() {

        for (int i = 0; i < envelopes.size(); i++) {
            if (Envelope.MISS.equals(envelopes.get(i))) {
                envelopes.remove(i);
                return;
            }
        }
    }
}
