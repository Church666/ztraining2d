package montyhall.yamada;

/**
 * 1回目に選択した封筒番号を2回目の選択時もそのまま選択する
 */
public class NoChangeSelectStrategy extends ASelectStrategy {

    /* 初回選択時の封筒番号. */
    private int firstSelectEnvelopNum;

    /**
     * @param firstSelectEnvelopNum 初回選択時に選んだ封筒番号.
     */
    public NoChangeSelectStrategy(int firstSelectEnvelopNum) {

        this.firstSelectEnvelopNum = firstSelectEnvelopNum;
    }

    /**
     * 2回目にクライアントが選択する封筒番号を返す.
     */
    @Override
    public int getSelectEnvelop() {

        return firstSelectEnvelopNum;
    }

}
