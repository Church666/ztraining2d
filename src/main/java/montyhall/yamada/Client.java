package montyhall.yamada;

import java.util.Random;

/**
 * 封筒を選択する
 *
 * @author yamada
 *
 */
public class Client {

    private int strategyNum;

    /**
     *
     * @param strategyNum ストラテジクラス番号
     */
    public Client(int strategyNum) {
        this.strategyNum = strategyNum;
    }

    /**
     * 初回選択時に選んだ封筒番号を返す
     *
     * @param envelopCnt 開始時の封筒数.
     * @return 初回選択時の封筒番号
     */
    public int getFirstSelectEnvelopNum(int envelopCnt) {

        Random rnd = new Random();

        int selectEnvelopNum = rnd.nextInt(envelopCnt);

        return selectEnvelopNum;
    }

    /**
     * 2回目の選択時に選んだ封筒番号を返す
     *
     * @param envelopCnt 開始時の封筒数
     * @param tearEnvelopNum モンティが破った封筒番号
     * @param firstSelectEnvelopNum 初回時に選択した封筒番号
     *
     * @return 2回目の選択時に選んだ封筒番号
     */
    public int getSecondSelectEnvelopNum(int envelopCnt, int tearEnvelopNum, int firstSelectEnvelopNum) {

        ASelectStrategy selectStrategy =
            StrategyFactory.getSelectStrategy(strategyNum, envelopCnt, tearEnvelopNum, firstSelectEnvelopNum);

        return selectStrategy.getSelectEnvelop();
    }
}
