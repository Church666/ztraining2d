package designpattern.strategy.homework;

/**
 * [レンタカー]予約実装クラス
 */
public class RentalCar implements IReserve {

    @Override
    public void reserve(Destination destination) {

        showDialog.outputMessage(Way.RENTALCAR.getValue() + "チケット予約します");

    }

}
