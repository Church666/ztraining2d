package designpattern.strategy.homework;

/**
 * [電車]予約実装クラス
 */
public class Train implements IReserve {

    @Override
    public void reserve(Destination destination) {

        showDialog.outputMessage(Way.TRAIN.getValue() + "チケット予約します");
    }

}
