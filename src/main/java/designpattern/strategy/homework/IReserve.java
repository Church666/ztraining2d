package designpattern.strategy.homework;

/**
 * 予約インターフェース
 */
public interface IReserve {
    ShowDialog showDialog = new ShowDialog();

    void reserve(Destination destination);
}
