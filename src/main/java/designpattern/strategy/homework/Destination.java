package designpattern.strategy.homework;

public enum Destination {

    JAPAN("国内"), AMERICA("アメリカ");

    String value = "";

    private Destination(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
