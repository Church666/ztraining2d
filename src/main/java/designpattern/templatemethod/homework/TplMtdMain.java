package designpattern.templatemethod.homework;

public class TplMtdMain {

    /**
     * @param args
     */
    public static void main(String[] args) {
        AbstractAAWriter tplMtd = new ConsoleAAWriter();
        String aaTitle = "セキセイインコ";
        String aaString =
            "　　　　　　　　　　　　　　　　ri'i'i'i'i'r -､\n" + "　　　　　　　　　　　　　　 ri{{{{{{{{{{ii:i: 　｀\n"
                + "　　　　　　　　　　　　　,ri{{{{{{{{{{{iiii:i:　　　i\n" + "　　　　　　　　　　　　rviiiviii{{{{iiii(●　 ｒﾆ')\n"
                + "　　　　　　　　　　　,ｲﾐviiミ≧≡ｒ‐､　　）ﾉ\n" + "　　　　　　　　　　,ｨミviviiiミ≧r'_ノ 　 f´\n" + "　　　　　　　　 ∠ミ≧≡≦､　　　 ﾐ ;;ﾉ\n"
                + "　　　　　　　 ｨ彡〃彡彡彡ヾ　　　 i´\n" + "、　、　　　　　 ｨ彡≠彡彡彡彡ｼ）　　 ;\n" + "、＼ ＼　　　 ｨ彡≠彡彡彡シシｼ　　 　'\n"
                + "＼ ＼ ＼　 ,:'　≠彡彡彡シシｼ　 　 　 '\n" + " ＼ ＼　 /　≠彡彡彡シシｼ　　　　 ,'\n" + "　 　＼　/　≠彡彡彡シシｼ　　 　　 ,'\n"
                + "　　 /　〃彡オ彡オ彡'　 　 　 　,'\n" + " 　 /／しオしオしシ　　　　　　/\n" + "　(ｼ(／／(シ(シノ　　　　　　, '\n" + "（／(／／／／　　　　　　／\n"
                + "（／(／／／／　　　　 ,,.,.,∠__\n" + "／／／／／　,／'\"'\"'\"\"|j弋￢)\n" + "（／／-'｀\"´ ,,／　　　　 　ゞ､ ＼ ＼\n"
                + "（／\"/　　　 ,,:' 　 　 　 　 　 　 ＼ ＼ ＼\n" + "/ ,　/ ,/　　　　　　　 　 　 　 ＼ ＼ ＼\n"
                + "/ / /,,\"　　　　　 　 　 　 　 　 　 ＼ ＼ ＼\n" + "/ / /\"　　　　　　　　　　　　 　 　 　 ＼ ＼ ＼\n"
                + ",/ / /　　　　　　　　　　　　 　 　 　 　 　 ＼ ＼ ＼\n" + "/ / /　　　　　　　　　　　　　　　　　　　　　　＼ ＼ ＼\n"
                + "（,（,_/ 　　　　　　　　　　　　　　　　 　 　 　 　 　 ＼ ＼ ＼\n";

        tplMtd.execute(aaTitle, aaString);
    }

}
