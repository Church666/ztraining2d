package designpattern.templatemethod.homework;

public class ConsoleAAWriter extends AbstractAAWriter {

    private StringBuffer buf = new StringBuffer();

    @Override
    protected void header(String aaTitle) {
        buf.append("【" + aaTitle + "】\n");
    }

    @Override
    protected void body(String aaString) {
        buf.append(aaString);
    }

    @Override
    protected void footer() {
        buf.append("====================================================");
    }

    @Override
    protected void write() {
        System.out.println(buf.toString());
    }
}
