package designpattern.iterator;

public interface Aggregate {
	public Iterator iterator();
}
