package designpattern.iterator;

public class Main {

	public static void main(String[] args) {
		BookShelf myBookShelf = new BookShelf(100);
		myBookShelf.appendBook(new Book("Janat"));
		myBookShelf.appendBook(new Book("Monet"));
		myBookShelf.appendBook(new Book("Cabernet"));
		myBookShelf.appendBook(new Book("珈琲"));

		Iterator it = myBookShelf.iterator();
		while (it.hasNext()) {
			Book book = (Book) it.next();
			System.out.println(book.getName());
		}

	}
}
