package designpattern.state.homework;

public class CloudyState implements IState {
    public void execute() {
        System.out.println("デートします");
    }
}