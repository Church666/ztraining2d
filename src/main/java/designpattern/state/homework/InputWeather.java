package designpattern.state.homework;

public class InputWeather {

    public IState getState(Weather inputWeather) {

        switch (inputWeather) {
            case SUNNY:
                return new SunnyState();
            case CLOUDY:
                return new CloudyState();
            case RAINY:
                return new RainyState();
            case SNOWY:
                return new SnowyState();
            default:
                return null;
        }
    }
}
