package designpattern.state.homework;

public class Main {

    private static Context context;

    public static void main(String[] args) {
        context = new Context();

        InputWeather inputWeather = new InputWeather();

        //        String r = "雪";

        //Weather a = Weather.getWeather(args[0]);
        //IState state = inputWeather.getState(Weather.getWeather(r));
        IState state = inputWeather.getState(Weather.getWeather(args[0]));

        execute(state);

        //		IState state = new SunnyState();
        //		execute(state);
        //
        //		state = new CloudyState();
        //		execute(state);
        //
        //		state = new RainyState();
        //		execute(state);
        //
        //		state = new SnowyState();
        //		execute(state);
    }

    private static void execute(IState state) {
        context.setState(state);
        context.request();
    }
}