package designpattern.state.homework;

public interface IState {
    public void execute();
}