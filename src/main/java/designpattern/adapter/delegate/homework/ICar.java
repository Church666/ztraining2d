package designpattern.adapter.delegate.homework;

/**
 * 車インターフェース
 */
public interface ICar {
    void run();

    void stop();
}
