package designpattern.adapter.delegate.homework;



/**
 * フェラーリクラス
 * @author jun.sawada
 */
public class Ferrari implements ICar {

    Signal signal = new Signal();

    @Override
    public void run() {
        signal.runSignal();
    }

    @Override
    public void stop() {
        signal.stopSignal();
    }

}
