package designpattern.adapter.delegate.homework;

/**
 * 合図クラス
 */
public class Signal {

    public void runSignal() {
        System.out.println("運転中ですよ");
    }

    public void stopSignal() {
        System.out.println("運転終了ですよ");
    }
}
