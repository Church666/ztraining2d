package designpattern.adapter.inherit.homework;

/**
 * ミュージシャン.
 */
public class Musician {

    /**
     * 歌う.
     */
    public void singSong() {
        System.out.println("♪♪♪");
    }
}
